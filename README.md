# NuGet .NET Core

Настройки для сборки NuGet пакета для .NET Core

В репозиторий требуется в `.gitlab-ci.yml` вставить код ниже:

```yml
variables:
  PackProjectName: Имя проекта для упаковки
  TestProjectName: Имя проекта для тестирования

include:
  - project: 'ci-cd-akasaka/nuget-.net-core'
    refs: master
    file: '/nuget-ci.yml'
```